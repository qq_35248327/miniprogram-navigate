Page({

  onLoad(options) {
    console.log('onLoad')
  },

  onShow() {
    console.log('onShow')
  },

  onReady() {
    console.log('onReady')
  },

  onHide() {
    console.log('onHide')
  },

  onUnload() {
    console.log('onUnload')
  },

  tapNavigateTo() {
    wx.navigateTo({ url: '/pages/normal-b/normal-b' })
  },

  tapRedirectTo() {
    wx.redirectTo({ url: '/pages/normal-b/normal-b' })
  },

  tapSwitchTab() {
    wx.switchTab({ url: '/pages/tab-b/tab-b' })
  },

  tapNavigateBack() {
    wx.navigateBack()
  },

  tapReLaunch() {
    wx.reLaunch({ url: '/pages/tab-a/tab-a' })
  },

  tapQuery() {
    wx.navigateTo({
      url: '/pages/normal-b/normal-b?name=张智琦&age=18'
    })
  }
})